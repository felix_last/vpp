var s1 = [[0, 27], [1, 27], [2, 27], [3, 27], [4, 16], [5, 16], [6, 22], [7, 22], [8, 0], [9, 0], [10, 0], [11, 0], [12, 30], [13, 30], [14, 30], [15, 30], [16, 30], [17, 26], [18, 26], [19, 26], [20, 26], [21, 26], [22, 26], [23, 26],[24, 26]] ;
var maxCapacity = [[0, 30], [1, 30], [2, 30], [3, 30], [4, 30], [5, 30], [6, 30], [7, 30], [8, 30], [9, 30], [10, 30], [11, 30], [12, 30], [13, 30], [14, 30], [15, 30], [16, 30], [17, 30], [18, 30], [19, 30], [20, 30], [21, 30], [22, 30], [23, 30],[24, 30]];
var plot1, hot1;
function drawGraph() { 
  if(plot1 != undefined) {
    plot1.destroy();
  }
  $.jqplot.config.enablePlugins = true;
 
  plot1 = $.jqplot('graph',[s1,maxCapacity],{
     //title: 'Highlighting, Dragging, Cursor and Trend Line',
     axes: {
         xaxis: {
          //renderer: $.jqplot.DateAxisRenderer,
          tickOptions: {
            //formatString: '%#m/%#d/%y'
          },
          //numberTicks: 12,
          min:0,
          max:24,
          tickInterval: 2, 
          tickOptions: { 
            formatString: '%d Uhr' 
          } 
         },
         yaxis: {
            min:0,
             tickOptions: {
                 formatString: '%d kW'
             }
         }
     },
     highlighter: {
         show: false,
         sizeAdjust: 10,
         tooltipLocation: 'n',
         tooltipAxes: 'both',
         //tooltipFormatString: '%d kW',
         useAxesFormatters: true
     },
     cursor: {
         show: true,
         style: 'default',
         tooltipAxes: 'both',
         tooltipLocation: 'ne'
     },
     series: [
        {
          color: 'rgba(140, 210, 240, 1)',
          dragable : {
            constrainTo:'none'
          },
          isDragable: false,
          label: 'voraussichtlicher Fahrplan'
        },
        {
          dragable : {
            //color : 'rgb(150,230,150)',
            constrainTo : 'y',
          },
          label: 'maximale Kapazität'
        }
      ],
      legend: {
        show: true,
        location: 'se',     // compass direction, nw, n, ne, e, se, s, sw, w.
        xoffset: 100,        // pixel offset of the legend box from the x (or x2) axis.
        yoffset: 40,        // pixel offset of the legend box from the y (or y2) axis.
    },
    grid: {
      background: 'white',
      borderWidth: 0,
      shadow: false
    }
  });
  
}

function updateEventType(event) {
  var typeName;
  if(event===undefined) {
    type = getURLParameter('event-type');
    if (type===undefined) {type="3";}
    switch(type) {
      case "1":
        typeName = "außerplanmäßige Wartung";
        break;
      case "2":
        typeName = "planmäßige Wartung";
        break;
      case "3": 
        typeName = "Sonstige";
        break;
    }
  }
  //typeName = $('#select-event-type option:selected').text();
  typeName = typeName[0].toUpperCase() + typeName.slice(1,typeName.length);
  $('.event-type').text( typeName );
}

function updateEventDate(event) {
  var startDate;
  if(event===undefined) {
    date = getURLParameter('start-date');
    if(date===undefined) {
      date = moment(new Date()).add(1, 'days');
    }
    startDate = moment(new Date(date));
  }
  $('.event-date').text( startDate.format('DD.MM.YYYY') );

}


function getURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
}

function initializeTable() {
  hot1 = new Handsontable(document.getElementById('data-grid'),{
    data: maxCapacity,
    //colWidths: [55, 80, 80, 80, 80, 80, 80], //can also be a number or a function
    rowHeaders: false,
    colHeaders: true,
    fixedColumnsLeft: 0,
    fixedRowsTop: 0,
    minSpareRows: 0,
    contextMenu: false,
    colHeaders: ['Uhrzeit', 'Leistung in kW'],
    stretchH: 'all',
    columns: [
      {
        readOnly: true,
        type: 'numeric'
      },
      {
        readOnly: false,
        type: 'numeric'
      }
    ],
    afterChange: function() {
        drawGraph();
    },
    outsideClickDeselects: false
  });
}

function maxCapacityDragged(sctx, options) {
  for(var i=0; i<plot1.series[1].data.length; i++) {
    maxCapacity[i][1] = Math.round( plot1.series[1].data[i][1] );
  }
  drawGraph();
  hot1.render();
  //initializeTable();
  //console.log('dragged');
}

function evaluateFormula(e) {
    var formula = $('#data-formula').val();
    // get selected numbers
    var selectedRange = hot1.getSelected(); //[2, 1, 4, 1]
    if(selectedRange[0]>selectedRange[2]) {
      var swap = selectedRange[0];
      selectedRange[0] = selectedRange[2];
      selectedRange[2] = swap;
    }
    for(i = selectedRange[0]; i <= selectedRange[2]; i++) {
      var value = hot1.getData(i,1,i,1)[0];
      var expression = value + formula;
      var result = value;
      try {
        result= eval(expression); 
        if(result<0) {result = 0;}
        hot1.setDataAtCell(i,1,result);   
      }
      catch(e) {
        alert("Syntax-Fehler in Werten oder Formel.");
        break;
      }

    }
}

$(document).ready(function() {
  drawGraph();
  $('#graph').bind('jqplotDragStop', maxCapacityDragged);
  initializeTable();

  updateEventType();
  $('#select-event-type').change(function(e) { updateEventType(e); });
  updateEventDate();
  $('#start-date').change(function(e) { updateEventDate(e); });

  $('#data-formula').keypress(function(e) {
    if(e.which == 13) {
      evaluateFormula(e);
    }
  });
  $('#execute-formula').click(evaluateFormula);
});