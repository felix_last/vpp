biogas = [[0, 27], [1, 27], [2, 27], [3, 27], [4, 16], [5, 16], [6, 22], [7, 22], [8, 0], [9, 0], [10, 0], [11, 0], [12, 30], [13, 30], [14, 30], [15, 30], [16, 30], [17, 26], [18, 26], [19, 26], [20, 26], [21, 26], [22, 26], [23, 26],[24, 26]] ;
optimization = [[7, 22], [8, 20], [9, 20], [10, 20], [11, 20], [12, 30]]  ;
optimized = [[0, 27], [1, 27], [2, 27], [3, 27], [4, 16], [5, 16], [6, 22], [7, 22], [8, 20], [9, 20], [10, 20], [11, 20], [12, 30], [13, 30], [14, 30], [15, 30], [16, 30], [17, 26], [18, 26], [19, 26], [20, 26], [21, 26], [22, 26], [23, 26], [24, 26]] ;

function drawGraph(data) { 
  $('#graph').empty();
  $.jqplot.config.enablePlugins = true;
 
  plot1 = $.jqplot('graph', data,{
     //title: 'Highlighting, Dragging, Cursor and Trend Line',
     axes: {
         xaxis: {
          //renderer: $.jqplot.DateAxisRenderer,
          tickOptions: {
            //formatString: '%#m/%#d/%y'
          },
          //numberTicks: 12,
          min:0,
          max:24,
          tickInterval: 2, 
          tickOptions: { 
            formatString: '%d Uhr' 
          } 
         },
         yaxis: {
            min:0,
             tickOptions: {
                 formatString: '%d kW'
             }
         }
     },
     highlighter: {
         sizeAdjust: 10,
         tooltipLocation: 'n',
         tooltipAxes: 'both',
         //tooltipFormatString: '%d kW',
         useAxesFormatters: true
     },
     cursor: {
         show: false,
         style: 'default',
         tooltipAxes: 'both'
     },
     series: [
        {
          isDragable: false,
          label: 'BHKW (Biogas)'
        },
        {
          label: 'Optimierung',
          isDragable: false,
          dragable : {
            constrainTo : 'y',
          },
          color: 'green'
        }
      ],
      legend: {
        show: true,
        location: 'se',     // compass direction, nw, n, ne, e, se, s, sw, w.
        xoffset: 0,        // pixel offset of the legend box from the x (or x2) axis.
        yoffset: 40,        // pixel offset of the legend box from the y (or y2) axis.
    },
    grid: {
      background: 'white',
      borderWidth: 0,
      shadow: false
    }
  });
}

$(document).ready(function() {
  $('#optimization-notice').hide();
  drawGraph([biogas]);
  $('#user-settings').click(function() {
      $('#optimization-notice').show();
      drawGraph([biogas, optimization]);
      //chart.load({columns:[['Optimierung',0,0,0,0,0,0,0,22,20,20,20,20,30,0,0,0,0,0,0,0,0,0,0]]});
  });
  $('#reject-optimization').click(function() {
      drawGraph([biogas]);
      $('#optimization-notice').hide();
  });
  $('#confirm-optimization').click(function() {
      drawGraph([optimized]);
      $('#optimization-notice').hide();
  });
});